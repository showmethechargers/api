# OsmType: This is either "node", "way" or "node,way" and indicates if this tag
# applies to nodes, ways, or both.
#
# Tag: The tag
#
# DataType: The type of the column to be created. Normally "text"
#
# Flags: Flags that indicate what table the OSM object is moved into.
#
# These flags are used both to indicate if a column should be created,
# and if ways with the tag are assumed to be areas. The area
# assumptions can be overridden with an area=yes/no tag
#
# polygon - Create a column for this tag, and objects with the tag are areas
#
# linear - Create a column for this tag
#
# nocolumn - Override the above and don't create a column for the tag, but do
# include objects with this tag
#
# delete - Drop this tag completely and don't create a column for it. This also
# prevents the tag from being added to hstore columns
#
# If an object has a tag that indicates it is an area or has area=yes/1,
# osm2pgsql will try to turn it into an area. If it succeeds, it places it in
# the polygon table. If it fails (e.g. not a closed way) it places it in the
# line table.
#
# Nodes are never placed into the polygon or line table and are always placed in
# the point table.

# OsmType  Tag                  DataType     Flags
node       amenity              text         linear
node       network              text         linear
node       access               text         linear
node       socket:type1         int4         linear
node       fee                  text         linear
node       parking:fee          text         linear
node       authentication:none  text         linear
node       network:wikidata     text         linear

node,way   z_order      int4         linear # This is calculated during import
way        way_area     real         linear # This is calculated during import

# Delete amenity because they are all charging_station
node       amenity                    text    delete

# Area tags
# We don't make columns for these tags, but objects with them are areas.
# Mainly for use with hstore
way         abandoned:aeroway       text    polygon,nocolumn
way         abandoned:amenity       text    polygon,nocolumn
way         abandoned:building      text    polygon,nocolumn
way         abandoned:landuse       text    polygon,nocolumn
way         abandoned:power         text    polygon,nocolumn
way         area:highway            text    polygon,nocolumn

# Deleted tags
# These are tags that are generally regarded as useless for most rendering.
# Most of them are from imports or intended as internal information for mappers
# Some of them are automatically deleted by editors.
# If you want some of them, perhaps for a debugging layer, just delete the lines.

# These tags are used by mappers to keep track of data.
# They aren't very useful for rendering.
node,way    note                    text    delete
node,way    note:*                  text    delete
node,way    attribution             text    delete
node,way    comment                 text    delete
node,way    fixme                   text    delete

# Tags generally dropped by editors, not otherwise covered
node,way    created_by              text    delete
node,way    odbl                    text    delete
node,way    odbl:note               text    delete
node,way    SK53_bulk:load          text    delete

# Lots of import tags
# TIGER (US)
node,way    tiger:*                 text    delete

# NHD (US)
# NHD has been converted every way imaginable
node,way    NHD:*                   text    delete
node,way    nhd:*                   text    delete

# GNIS (US)
node,way    gnis:*                  text    delete

# Geobase (CA)
node,way    geobase:*               text    delete
# NHN (CA)
node,way    accuracy:meters         text    delete
node,way    sub_sea:type            text    delete
node,way    waterway:type           text    delete

# KSJ2 (JA)
# See also note:ja and source_ref above
node,way    KSJ2:*                  text    delete
# Yahoo/ALPS (JA)
node,way    yh:*                    text    delete

# osak (DK)
node,way    osak:*                  text    delete

# kms (DK)
node,way    kms:*                   text    delete

# ngbe (ES)
# See also note:es and source:file above
node,way    ngbe:*                  text    delete

# naptan (UK)
node,way    naptan:*                text    delete

# Corine (CLC) (Europe)
node,way    CLC:*                   text    delete

# misc
node,way    3dshapes:ggmodelk       text    delete
node,way    AND_nosr_r              text    delete
node,way    import                  text    delete
node,way    it:fvg:*                text    delete
