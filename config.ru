# frozen_string_literal: true

# Copyright (C) 2019 by Stephen Paul Weber
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "pg"
require "json"
require "plus_codes/open_location_code"

DB = PG.connect(dbname: "smtc")

if defined?(PhusionPassenger)
	PhusionPassenger.on_event(:starting_worker_process) do |forked|
		DB = PG.connect(dbname: "smtc") if forked
	end
end

class ChargingStation
	def initialize(db_row)
		@row = db_row
	end

	def plus_code
		PlusCodes::OpenLocationCode.new.encode(
			@row["latitude"].to_f,
			@row["longitude"].to_f,
			11
		)
	end

	def to_json
		JSON.dump(
			type:     "Feature",
			geometry: {
				type:        "Point",
				coordinates: [@row["longitude"].to_f, @row["latitude"].to_f]
			},
			id:       @row["osm_id"]
		)[0..-2] +
		",\"properties\": #{@row["tags"][0..-2]}" +
		", \"addr:olc\": #{JSON.dump(plus_code)}}}"
	end
end

class Query
	SQL = <<~SQL
		SELECT
			osm_id,
			ST_X(ST_Transform(way,4326)) as longitude,
			ST_Y(ST_Transform(way,4326)) as latitude,
			to_json(tags || hstore('network:wikidata:label', networks.label)) as tags
		FROM
			planet_osm_point
		LEFT JOIN
			networks ON "network:wikidata"=networks.id
		WHERE
			way && ST_Transform(ST_MakeEnvelope($1,$2,$3,$4,4326),3857)
	SQL

	def initialize(bbox)
		@bbox = bbox
	end

	def each
		yield "[\n"
		DB.exec_params(SQL, @bbox) do |result|
			result.each.with_index do |row, idx|
				yield ",\n" unless idx.zero?
				yield "\t"
				yield ChargingStation.new(row).to_json
			end
		end
		yield "\n]\n"
	end
end

run lambda { |env|
	lat1, long1, lat2, long2 = env["QUERY_STRING"].split(/,/, 4)
	[
		200,
		{
			"Access-Control-Allow-Origin" => "*",
			"Content-Type"                => "application/json"
		},
		Query.new([long1, lat1, long2, lat2])
	]
}
