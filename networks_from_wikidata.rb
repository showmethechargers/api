#!/usr/bin/ruby

require "pg"
require "open-uri"
require "json"

DB = PG.connect(dbname: "smtc")

TABLE = <<~SQL
	CREATE TABLE IF NOT EXISTS networks (
		id TEXT PRIMARY KEY,
		label TEXT,
		wikidata JSONB
	);
SQL

QUERY = <<~SQL
	SELECT
		DISTINCT "network:wikidata" AS wikidata
	FROM
		planet_osm_point
	WHERE
		"network:wikidata" IS NOT NULL;
SQL

UPSERT = <<~SQL
	INSERT INTO networks (id, label, wikidata)
	VALUES ($1, $2, $3)
	ON CONFLICT (id)
	DO UPDATE SET label=$2, wikidata=$3;
SQL

DB.exec(TABLE)

DB.exec(QUERY).each do |row|
	wikidata = JSON.parse(
		open("https://www.wikidata.org/entity/#{row["wikidata"]}.json").read
	)

	DB.exec_params(UPSERT, [
		row["wikidata"],
		wikidata.dig("entities", row["wikidata"], "labels", "en", "value"),
		JSON.dump(wikidata.dig("entities", row["wikidata"]))
	])
end
